import React from 'react';
import styled from "react-emotion";
import {smallSpacing} from '../Common/Styles';
import RowContainer from "./RowContainer";

const Label = styled('label')`
  margin-right: ${smallSpacing};
`;

const LabeledRow = ({label, children}) => <RowContainer>
  <Label>{label}:</Label>
  {children}
</RowContainer>;

export default LabeledRow;