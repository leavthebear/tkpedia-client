import React from 'react';
import styled, {css} from "react-emotion";
import {smallSpacing} from '../Common/Styles';
import RowContainer from "./RowContainer";

const marginStyle = ({margin}) => css`
  margin: 0 ${margin};
`;
const SeparatorContainer = styled('span')`
  ${marginStyle};
`;

export const Separator = ({separator, margin}) => <SeparatorContainer margin={margin}>{separator}</SeparatorContainer>;

export const SeparatedList = ({separator, margin, children}) => <RowContainer>
  {children.reduce((prev, current, index) => {
    return [prev, <Separator key={"Separator" + index} separator={separator ? separator : '•'} margin={margin ? margin : smallSpacing}/>, current];
  })}
</RowContainer>;

export function join(children, separator='•', margin=smallSpacing) {
  return children.reduce((prev, current, index) => {
    return [prev, <Separator key={"Separator" + index} separator={separator} margin={margin}/>, current];
  });
}

export default SeparatedList;
