import React from 'react';
import LabeledRow from './LabeledRow';
import SeparatedList from "./SeparatedList";

const NickNames = ({nicknames}) => <LabeledRow label={'别称'}>
  <SeparatedList>
    {nicknames.map((nickname, index) => <span key={index}>{nickname}</span>)}
  </SeparatedList>
</LabeledRow>;

export default NickNames;