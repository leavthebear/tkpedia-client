import React from 'react';
import LabeledRow from "./LabeledRow";

function buildTable(sizeTable) {
  let unit = '';
  if (sizeTable.dimensions && sizeTable.dimensions.length > 0) {
    unit = sizeTable.dimensions[0].unit
  }
  const table = [[unit].concat(sizeTable.sizes)];

  sizeTable.dimensions.forEach(dimension => {
    const row = [dimension.name].concat(dimension.values);
    table.push(row);
  });


  return table;
}

// const result = {
//   sizes: ["M", "L"],
//   dimensions: [
//     {name: "胸围", unit: "cm", values: [85, 94]},
//     {name: "腰围", unit: "cm", values: [64, 74]},
//   ],
// };

const SizeTable = ({sizeTable}) => {
  const table = buildTable(sizeTable);
  return <LabeledRow label="尺码">
    <table>
      <tbody>
      {
        table.map((row, index) => <tr key={index}>
          {
            row.map((cell, index) => <td key={index}>
              {cell}
            </td>)
          }
        </tr>)
      }
      </tbody>
    </table>
  </LabeledRow>
};

export default SizeTable;