// Returns a response containing a list of items and the next page if available.
import moment from "moment"

export function listItems(page = 1) {
  return fetch(`https://tiankeng.storytea.top/wp-json/wp/v2/zitem?page=${page}`).then(response => {
    const totalPagesString = response.headers.get("x-wp-totalpages");
    if (!totalPagesString) {
      return Promise.reject("missing x-wp-totalpages header in the response");
    }
    const totalPages = parseInt(totalPagesString, 10);
    let nextPage = null;
    if (totalPages && page < totalPages) {
      nextPage = () => listItems(page + 1);
    }
    return response.json()
        .then(json => Promise.all(json.map(itemJson => parseItem(itemJson))))
        .then(items => ({items, nextPage}))
  });
}

export function getItem(id) {
  return fetch(`https://tiankeng.storytea.top/wp-json/wp/v2/zitem/${id}`)
      .then(response => response.ok ? response.json()
          .then(json => parseItem(json)) : Promise.reject(`unable to get item with ID ${id}: ${response}`))
}

// const sameple = {
//   "id": 362,
//   "date": "2018-08-30T16:34:39",
//   "date_gmt": "2018-08-30T08:34:39",
//   "guid": {
//     "rendered": "https://tiankeng.storytea.top/?post_type=zitem&#038;p=362"
//   },
//   "modified": "2018-08-31T08:55:45",
//   "modified_gmt": "2018-08-31T00:55:45",
//   "slug": "%e7%8e%9b%e6%a0%bc%e4%b8%bd%e7%89%b9-2016-jsk-2",
//   "status": "publish",
//   "type": [
//     "JSK"
//   ],
//   "link":
//       "https://tiankeng.storytea.top/archives/zitem/%e7%8e%9b%e6%a0%bc%e4%b8%bd%e7%89%b9-2016-jsk-2",
//   "title": {"rendered": "玛格丽特 (2016)  | JSK"},
//   "content": {
//     "rendered": "", "protected": false
//   },
//   "featured_media": 310,
//   "comment_status": "open",
//   "ping_status": "closed",
//   "template": "",
//   "meta": [],
//   "image_list": []
//   ,
//   "name":
//       [" 玛格丽特 (2016)"],
//   "ori_name":
//       ["Margaretha ジャンパースカート"],
//   "brand_id":
//       ["328"
//       ],
//   "other_name_list":
//       ["[\"药箱JSK\"]"],
//   "price_json":
//       [
//         "{\"price_set\":[{\"type\":\"定金价\",\"unit\":\"JPY\",\"num\":\"500\"},{\"type\":\"尾款价\",\"unit\":\"JPY\",\"num\":\"2500\"},{\"type\":\"现货价\",\"unit\":\"JPY\",\"num\":\"3000\"},{\"type\":\"Sale价\",\"unit\":\"JPY\",\"num\":\"2800\"}]}"
//       ],
//   "date_json":
//       [
//         "{\"dataset\":[{\"time\":\"2017/07/18\",\"thing\":\"设计图透\"},{\"time\":\"2017/07/18\",\"thing\":\"实物图透\"},{\"time\":\"2017/08/18\",\"thing\":\"预约\"},{\"time\":\"2017/09/19\",\"thing\":\"尾款\"},{\"time\":\"2017/11/08\",\"thing\":\"现货\"},{\"time\":\"2018/08/08\",\"thing\":\"完售\"}]}"
//       ],
//   "color_json":
//       [
//         "{\"colorset\":[{\"color\":\"#b6e5c1,#efefe6\",\"name\":\"若草x纯白\"},{\"color\":\"#7c202d,#efefe6\",\"name\":\"酒红x纯白\"},{\"color\":\"#14472f,#efefe6\",\"name\":\"墨绿x纯白\"},{\"color\":\"#a0a0a0,#efefe6\",\"name\":\"绀x纯白\"}]}"
//       ],
//   "size_json":
//       ["[[\"(cm)\",\"M\",\"L\"],[\"胸围\",\"85\",\"94\"],[\"腰围\",\"65\",\"74\"]]"
//       ],
//   "type_list":
//       ["[\"全素鸡\",\"抽带\",\"左侧拉链\",\"胸口可拆卸蝴蝶结\"]"],
//   "cloth_type_list":
//       ["[\"棉\",\"麻\",\"化纤\"]"],
//   "user_tag_list":
//       ["[\"护士\",\"十字架\",\"纯色\",\"萌款\"]"],
//   "designer_list":
//       ["9"],
//   "shop_json":
//       ["{\"shopset\":[{\"type\":\"购买链接\",\"link\":\"www.google.com\"},{\"type\":\"代购商1\",\"link\":\"www.google.com\"},{\"type\":\"代购商2\",\"link\":\"www.google.com\"}]}"],
//   "shop_disable":
//       false,
//   "vote_result":
//       "{\"luguo\":\"0\",\"woyou\":\"0\",\"zhcao\":\"1\",\"yding\":\"0\",\"cengy\":\"0\"}",
//   "_links":
//       {
//         "self":
//             [{"href": "https://tiankeng.storytea.top/wp-json/wp/v2/zitem/362"}], "collection":
//             [{"href": "https://tiankeng.storytea.top/wp-json/wp/v2/zitem"}], "about":
//             [{"href": "https://tiankeng.storytea.top/wp-json/wp/v2/types/zitem"}], "replies":
//             [{
//               "embeddable": true,
//               "href": "https://tiankeng.storytea.top/wp-json/wp/v2/comments?post=362"
//             }], "version-history":
//             [{
//               "count": 2,
//               "href": "https://tiankeng.storytea.top/wp-json/wp/v2/zitem/362/revisions"
//             }], "predecessor-version":
//             [{
//               "id": 364,
//               "href": "https://tiankeng.storytea.top/wp-json/wp/v2/zitem/362/revisions/364"
//             }], "wp:featuredmedia":
//             [{
//               "embeddable": true,
//               "href": "https://tiankeng.storytea.top/wp-json/wp/v2/media/310"
//             }], "wp:attachment":
//             [{"href": "https://tiankeng.storytea.top/wp-json/wp/v2/media?parent=362"}], "curies":
//             [{"name": "wp", "href": "https://api.w.org/{rel}", "templated": true}]
//       }
// };

function parseItem(json) {
  return promiseProps({
    id: parseID(json.id),
    createdAt: parseCreatedAt(json.date_gmt),
    name: parseName(json.name),
    type: parseType(json.type),
    otherNames: parseOtherNames(json.ori_name),
    featureImage: parseFeaturedImage(json["wp:featuredmedia"]),
    images: parseImages(json.image_list),
    brand: parseBrand(json.brand_id),
    nicknames: parseNicknames(json.other_name_list),
    prices: parsePrices(json.price_json),
    dates: parseDates(json.date_json),
    colors: parseColors(json.color_json),
    traits: parseTraits(json.type_list),
    sizeTable: parseSizeTable(json.size_json),
    materials: parseMaterials(json.cloth_type_list),
    userTags: parseUserTags(json.user_tag_list),
    designers: parseDesigners(json.designer_list),
    links: parseLinks(json.shop_json),
  });
  //
  //
  // id: json.id,
  // name: json.title.rendered,
  // otherNames: "TODO",
  // images: [],
  // brand: {
  //   icon: 'TODO',
  //   name: 'TODO',
  // },
  // nicknames: ['药箱JSK', 'Margaretha'],
  // prices: [{
  //   amount: 3000, currency: 'JPY', note: '现货价',
  // }, {
  //   amount: 200, currency: 'USD', note: '二手价',
  // },],
  // dates: [{
  //   timestamp: new Date(), note: '设计图透',
  // }, {
  //   timestamp: new Date(), note: '实物图透',
  // },],
  // colors: [[{
  //   color: '#c3d825', name: '若草'
  // }, {
  //   color: '#ffffff', name: '白',
  // }], [{
  //   color: '#800000', name: '酒红'
  // }, {
  //   color: '#ffffff', name: '白',
  // }], [{
  //   color: '#274d3d', name: '墨绿'
  // }, {
  //   color: '#ffffff', name: '白',
  // }], [{
  //   color: '#003a6c', name: '绀'
  // }, {
  //   color: '#ffffff', name: '白',
  // }]],
  // sizeTable: {
  //   unit: 'cm', dimensions: [{
  //     name: '胸围', sizes: [{name: 'M', value: '85'}, {name:
  // 'L', value: '94'},] }, { name: '腰围', sizes: [{name: 'M',
  // value: '64'}, {name: 'L', value: '74'},] }, { name: '臀围',
  // sizes: [{name: 'L', value: '95'},] }] }, materials: ['棉',
  // '麻', '化纤'], userTags: ['护士', '十字架', '纯色', '草莓', '萌款',
  // '神款'], designers: [{ name: "孙大喵什么都想玩", image: userImage0,
  // }, { name: "折鹤", image: userImage1, },],
}

const parseID = field => field;
const parseCreatedAt = field => moment.utc(field, 'YYYY-MM-DDTHH:mm:ss');
const parseName = field => firstOfArray(field);
const parseType = field => firstOfArray(field);
const parseOtherNames = field => field;
//       "wp:featuredmedia": [
//         {
//           "embeddable": true,
//           "href": "https://tiankeng.storytea.top/wp-json/wp/v2/media/310"
//         }
//       ],
const parseFeaturedImage = field => {
}; //TODO
//     "image_list": [
//       "https://tiankeng.storytea.top/wp-content/uploads/2018/08/028e2fab-3db4-5bdf-9e7c-666400f53083.jpeg
// https://tiankeng.storytea.top/wp-content/uploads/2018/08/fb12f7fb-999f-566e-a8f4-5cea535a0953.jpeg
// https://tiankeng.storytea.top/wp-content/uploads/2018/08/8a451867-4fc9-557b-832a-b2c788217ea6.jpeg
// https://tiankeng.storytea.top/wp-content/uploads/2018/08/b0453345-c21b-5334-b613-902222d33205.jpeg
// https://tiankeng.storytea.top/wp-content/uploads/2018/08/50f271bc-61eb-5fef-978d-27ae0aede251.jpeg
// https://tiankeng.storytea.top/wp-content/uploads/2018/08/b4a2aeed-c7f9-5447-9817-98d9e1ab3a8e.jpeg
// https://tiankeng.storytea.top/wp-content/uploads/2018/08/e0806930-2c3a-5a8f-9d61-c4acfbd716f2.jpeg
// https://tiankeng.storytea.top/wp-content/uploads/2018/08/4cd5e00a-5f44-5374-8d1c-b4a537b3c7a2.jpeg
// https://tiankeng.storytea.top/wp-content/uploads/2018/08/397b62f2-3adb-5258-9c7c-1a00a4295ff8.jpeg
// https://tiankeng.storytea.top/wp-content/uploads/2018/08/2fdcb8b0-0d6d-569c-b26e-3502e9c67566.jpeg
// https://tiankeng.storytea.top/wp-content/uploads/2018/08/f4bd9920-b81b-5dcc-abed-3cb15de99d3c.jpeg https://tiankeng.storytea.top/wp-content/uploads/2018/08/f7394578-5696-5422-9741-d9f6958c3b78.jpeg" ],
const parseImages = field => firstOfArray(field, '').split(' ').map(url => ({full: {url: url}}));
const parseBrand = field => fetchBrand(firstOfArray(field));
const parseNicknames = field => JSON.parse(firstOfArray(field));
const parsePrices = field => JSON.parse(firstOfArray(field, '{"price_set":[]}')).price_set.map(price => ({
  amount: price.num, currency: price.unit, type: price.type,
}));
const parseDates = field => JSON.parse(firstOfArray(field, '{"dataset":[]}')).dataset.map(date => ({
  timestamp: moment.utc(date.time, 'YYYY/MM/DD'), note: date.thing,
}));
//     "color_json": [
//       "{\"colorset\":[{\"color\":\"#b6e5c1,#efefe6\",\"name\":\"若草x纯白\"},{\"color\":\"#7c202d,#efefe6\",\"name\":\"酒红x纯白\"},{\"color\":\"#14472f,#efefe6\",\"name\":\"墨绿x纯白\"},{\"color\":\"#a0a0a0,#efefe6\",\"name\":\"绀x纯白\"}]}"
// ],
const parseColors = field => JSON.parse(firstOfArray(field, '{"colorset":[]}')).colorset
    .map(combination => {
      const colors = combination.color.split(',');
      const names = combination.name.split('x');
      return colors.map((color, index) => ({
        color,
        name: index < names.length ? names[index] : ''
      }));
    });
//     "type_list": [
//       "[\"全素鸡\",\"抽带\",\"左侧拉链\",\"胸口可拆卸蝴蝶结\"]"
//     ],
const parseTraits = field => JSON.parse(firstOfArray(field));
//     "size_json": [
//       "[[\"(cm)\",\"胸围\",\"腰围\"],[\"M\",\"85\",\"65\"],[\"L\",\"94\",\"74\"]]"
//     ],
const parseSizeTable = field => {
  const table = JSON.parse(firstOfArray(field));
  if (!table || table.length === 0) {
    return null;
  }
  // const result = {
  //   sizes: ["M", "L"],
  //   dimensions: [
  //     {dimension: "胸围", unit: "cm", values: [85, 94]},
  //     {dimension: "腰围", unit: "cm", values: [64, 74]},
  //   ],
  // };
  const unit = table[0][0];
  const dimensions = table[0].slice(1);
  const sizeRows = table.slice(1);
  const sizes = sizeRows.map(row => row[0]);
  return {
    sizes, dimensions: dimensions.map((name, index) => ({
      name, unit, values: sizeRows.map(row => row[index + 1]),
    })),
  };
};
//     "cloth_type_list": [
//       "[\"棉\",\"麻\",\"化纤\"]"
//     ],
const parseMaterials = field => JSON.parse(firstOfArray(field, '[]'));
//   "user_tag_list":
//       ["[\"护士\",\"十字架\",\"纯色\",\"萌款\"]"],
const parseUserTags = field => JSON.parse(firstOfArray(field, '[]'));
const parseDesigners = field => [];
const parseLinks = field => [];

function firstOfArray(array, defaultValue = null) {
  if (array && array.length > 0) {
    return array[0];
  }
  return defaultValue;
}

function fetchBrand(id) {
  if (!id) {
    return Promise.resolve(null);
  }
  return fetch(`https://tiankeng.storytea.top/wp-json/wp/v2/brand/${id}`).then(response => response.json().then(json => fetchMedia(json.featured_media).then(media => ({
    id: json.id, name: json.title.rendered, image: media,
  }))));
}

function fetchMedia(id) {
  return fetch(`https://tiankeng.storytea.top/wp-json/wp/v2/media/${id}`).then(response => response.ok ? response.json().then(json => ({
    thumbnail: parseImageSize(json.media_details.sizes.thumbnail),
    medium: parseImageSize(json.media_details.sizes.medium),
    full: parseImageSize(json.media_details.sizes.full),
  })) : null);
}

function parseImageSize(json) {
  if (json) {
    return {
      width: json.width, height: json.height, url: json.source_url,
    };
  }
  return null;
}

// https://tiankeng.storytea.top/wp-json/wp/v2/media/274
// const brandMedia = {
//   "id": 274,
//   "date": "2018-08-09T23:30:06",
//   "date_gmt": "2018-08-09T15:30:06",
//   "guid": {"rendered":
// "https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic_cover.jpg"},
// "modified": "2018-08-09T23:30:06", "modified_gmt": "2018-08-09T15:30:06", "slug":
// "popteamepic_cover", "status": "inherit", "type": "attachment", "link":
// "https:\/\/tiankeng.storytea.top\/archives\/brand\/pop-team-epic\/popteamepic_cover", "title":
// {"rendered": "popteamepic_cover"}, "author": 1, "comment_status": "open", "ping_status":
// "closed", "template": "", "meta": [], "description": {"rendered": "<p class=\"attachment\"><a
// href='https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic_cover.jpg'><img
// width=\"300\" height=\"300\"
// src=\"https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic_cover-300x300.jpg\"
// class=\"attachment-medium size-medium\" alt=\"\"
// srcset=\"https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic_cover-300x300.jpg
// 300w,
// https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic_cover-150x150.jpg
// 150w, https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic_cover.jpg
// 451w\" sizes=\"(max-width: 300px) 85vw, 300px\" \/><\/a><\/p>\n"}, "caption": {"rendered": ""},
// "alt_text": "", "media_type": "image", "mime_type": "image\/jpeg", "media_details": { "width":
// 451, "height": 450, "file": "2018\/08\/popteamepic_cover.jpg", "sizes": { "thumbnail": { "file":
// "popteamepic_cover-150x150.jpg", "width": 150, "height": 150, "mime_type": "image\/jpeg",
// "source_url": "https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic_cover-150x150.jpg" }, "medium": { "file": "popteamepic_cover-300x300.jpg", "width": 300, "height": 300, "mime_type": "image\/jpeg", "source_url": "https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic_cover-300x300.jpg" }, "full": { "file": "popteamepic_cover.jpg", "width": 451, "height": 450, "mime_type": "image\/jpeg", "source_url": "https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic_cover.jpg" } }, "image_meta": { "aperture": "0", "credit": "", "camera": "", "caption": "", "created_timestamp": "0", "copyright": "", "focal_length": "0", "iso": "0", "shutter_speed": "0", "title": "", "orientation": "0", "keywords": [] } }, "post": 272, "source_url": "https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic_cover.jpg", "_links": { "self": [{ "attributes": [], "href": "https:\/\/tiankeng.storytea.top\/wp-json\/wp\/v2\/media\/274" }], "collection": [{ "attributes": [], "href": "https:\/\/tiankeng.storytea.top\/wp-json\/wp\/v2\/media" }], "about": [{ "attributes": [], "href": "https:\/\/tiankeng.storytea.top\/wp-json\/wp\/v2\/types\/attachment" }], "author": [{ "attributes": {"embeddable": true}, "href": "https:\/\/tiankeng.storytea.top\/wp-json\/wp\/v2\/users\/1" }], "replies": [{ "attributes": {"embeddable": true}, "href": "https:\/\/tiankeng.storytea.top\/wp-json\/wp\/v2\/comments?post=274" }] } }

// const brand = {
//   "id": 272,
//   "date": "2018-08-09T23:30:19",
//   "date_gmt": "2018-08-09T15:30:19",
//   "guid": {"rendered": "https:\/\/tiankeng.storytea.top\/?post_type=brand&#038;p=272"},
//   "modified": "2018-08-09T23:30:19",
//   "modified_gmt": "2018-08-09T15:30:19",
//   "slug": "pop-team-epic",
//   "status": "publish",
//   "type": "brand",
//   "link": "https:\/\/tiankeng.storytea.top\/archives\/brand\/pop-team-epic",
//   "title": {"rendered": "POP TEAM EPIC"},
//   "content": {
//     "rendered":
// "<p>\u300apop\u5b50\u548cpipi\u7f8e\u7684\u65e5\u5e38\u300b\u662f\u7531\u65e5\u672c\u6f2b\u753b\u5bb6\u5927\u5ddd\u3076\u304f\u3076\u521b\u4f5c\u7684\u559c\u5267\u56db\u683c\u6f2b\u753b\u4f5c\u54c1\uff0c\u4e8e2014\u5e7411\u670829\u65e5\u5728\u6f2b\u753b\u7f51\u7ad9\u201cMANGA
// LIFE
// WIN\u201d\u4e0a\u8fde\u8f7d\u3002\u6f2b\u753b\u5355\u884c\u672c\u7531\u7af9\u4e66\u623f\u51fa\u7248\uff0c\u5e76\u6709\u7535\u89c6\u52a8\u753b\u7b49\u884d\u751f\u4f5c\u54c1\u3002<\/p>\n<p><img
// class=\"alignnone size-full wp-image-273\"
// src=\"https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic.jpg\" alt=\"\"
// width=\"1080\" height=\"609\"
// srcset=\"https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic.jpg 1080w,
// https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic-300x169.jpg 300w,
// https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic-768x433.jpg 768w,
// https:\/\/tiankeng.storytea.top\/wp-content\/uploads\/2018\/08\/popteamepic-1024x577.jpg 1024w\"
// sizes=\"(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px\" \/><\/p>\n", "protected": false }, "featured_media": 274, "comment_status": "closed", "ping_status": "closed", "template": "", "meta": [], "web": ["https:\/\/zh.moegirl.org\/POP_TEAM_EPIC"], "short_label": ["\u751f\u6c14\u4e86\u5417\uff1f\u6ca1\u6709\u54e6"], "_links": { "self": [{"href": "https:\/\/tiankeng.storytea.top\/wp-json\/wp\/v2\/brand\/272"}], "collection": [{"href": "https:\/\/tiankeng.storytea.top\/wp-json\/wp\/v2\/brand"}], "about": [{"href": "https:\/\/tiankeng.storytea.top\/wp-json\/wp\/v2\/types\/brand"}], "replies": [{ "embeddable": true, "href": "https:\/\/tiankeng.storytea.top\/wp-json\/wp\/v2\/comments?post=272" }], "wp:featuredmedia": [{ "embeddable": true, "href": "https:\/\/tiankeng.storytea.top\/wp-json\/wp\/v2\/media\/274" }], "wp:attachment": [{"href": "https:\/\/tiankeng.storytea.top\/wp-json\/wp\/v2\/media?parent=272"}], "curies": [{"name": "wp", "href": "https:\/\/api.w.org\/{rel}", "templated": true}] } }

function promiseProps(object) {
  let promisedProperties = [];
  const objectKeys = Object.keys(object);

  objectKeys.forEach((key) => promisedProperties.push(object[key]));

  return Promise.all(promisedProperties)
      .then((resolvedValues) => {
        return resolvedValues.reduce((resolvedObject, property, index) => {
          resolvedObject[objectKeys[index]] = property;
          return resolvedObject;
        }, object);
      });

}
