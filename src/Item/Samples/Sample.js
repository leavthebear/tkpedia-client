import uuidV4 from '../../Common/UUID';
import userImage0 from './userImage0.png';
import userImage1 from './userImage1.png';

const sample = {
  id: uuidV4(),
  name: '玛格丽特 (2016) | JSK',
  otherNames: ['Margaretha'],
  images: ['https://images.lolibrary.org/file/lolibrary-images/b1d98c8a-d67a-5ffa-b353-ac595334aebd.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/275e16a7-ddff-5172-9f74-8c557d618227.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/ce979bf7-d5c0-5c01-b04f-9e8668f20b5f.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/b8ad6026-2f06-5fbd-9f44-ea0a233d2880.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/b0453345-c21b-5334-b613-902222d33205.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/8a451867-4fc9-557b-832a-b2c788217ea6.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/fb12f7fb-999f-566e-a8f4-5cea535a0953.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/4cd5e00a-5f44-5374-8d1c-b4a537b3c7a2.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/e0806930-2c3a-5a8f-9d61-c4acfbd716f2.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/b4a2aeed-c7f9-5447-9817-98d9e1ab3a8e.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/50f271bc-61eb-5fef-978d-27ae0aede251.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/f4bd9920-b81b-5dcc-abed-3cb15de99d3c.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/397b62f2-3adb-5258-9c7c-1a00a4295ff8.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/2fdcb8b0-0d6d-569c-b26e-3502e9c67566.jpeg', 'https://images.lolibrary.org/file/lolibrary-images/c4182096-232d-5efc-9d50-bc4f298fcd86.jpeg',],
  brand: {
    icon: 'https://pbs.twimg.com/profile_images/875928971241693184/srlNJySw_400x400.jpg',
    name: 'Baby, the Stars Shine Bright',
  },
  nicknames: ['药箱JSK', 'Margaretha'],
  prices: [{
    amount: 3000, currency: 'JPY', note: '现货价',
  }, {
    amount: 200, currency: 'USD', note: '二手价',
  },],
  dates: [{
    timestamp: new Date(), note: '设计图透',
  }, {
    timestamp: new Date(), note: '实物图透',
  },],
  colors: [[{
    color: '#c3d825', name: '若草'
  }, {
    color: '#ffffff', name: '白',
  }], [{
    color: '#800000', name: '酒红'
  }, {
    color: '#ffffff', name: '白',
  }], [{
    color: '#274d3d', name: '墨绿'
  }, {
    color: '#ffffff', name: '白',
  }], [{
    color: '#003a6c', name: '绀'
  }, {
    color: '#ffffff', name: '白',
  }]],
  sizeTable: {
    unit: 'cm', dimensions: [{
      name: '胸围', sizes: [{name: 'M', value: '85'}, {name: 'L', value: '94'},]
    }, {
      name: '腰围', sizes: [{name: 'M', value: '64'}, {name: 'L', value: '74'},]
    }, {
      name: '臀围', sizes: [{name: 'L', value: '95'},]
    }]
  },
  materials: ['棉', '麻', '化纤'],
  userTags: ['护士', '十字架', '纯色', '草莓', '萌款', '神款'],
  designers: [{
    name: "孙大喵什么都想玩", image: userImage0,
  }, {
    name: "折鹤", image: userImage1,
  },],
};

export default sample;
