import React from 'react';
import styled from "react-emotion";
import LabeledRow from "./LabeledRow";
import {smallSpacing, tinySpacing} from "../Common/Styles";

const UserTag = styled('div')`
  background-color: #cccccc;
  border-radius: ${smallSpacing};
  margin: ${tinySpacing};
  padding: ${smallSpacing};
`;

const UserTags = ({userTags}) => <LabeledRow label="用户标签">
  {userTags.map((userTag, index) => <UserTag key={index}>{userTag}</UserTag>)}
</LabeledRow>;

export default UserTags;