import React from 'react';
import LabeledRow from './LabeledRow';
import SeparatedList, {join} from "./SeparatedList";
import RowContainer from "./RowContainer";
import styled, {css} from "react-emotion"
import {tinySpacing} from "../Common/Styles";

// colors: [[{
//   color: '#c3d825', name: '若草'
// }, {
//   color: '#ffffff', name: '白',
// }], [{
//   color: '#800000', name: '酒红'
// }, {
//   color: '#ffffff', name: '白',
// }]],


const backgroundColorStyle = ({color}) => css`
  background-color: ${color};
`;
const ColorSample = styled('div')`
  ${backgroundColorStyle};
  width: 0.5em;
  height: 0.5em;
  align-self: center;
  border-style: solid;
  border-width: 0.05em;
  margin: auto 0.2em;
`;

const ColorInfo = ({colorInfo}) => <RowContainer>
  <ColorSample color={colorInfo.color}/>
  <div>{colorInfo.name}</div>
</RowContainer>;

const ColorComposition = ({colorComposition}) => <SeparatedList separator="x" margin={tinySpacing}>
  {
    colorComposition.map((colorInfo, index) => <ColorInfo key={index} colorInfo={colorInfo}/>)
  }
</SeparatedList>;

const Colors = ({colors}) => <LabeledRow label={'颜色'}>
    {join(colors.map((colorComposition, index) => <ColorComposition key={index} colorComposition={colorComposition}/>))}
</LabeledRow>;

export default Colors;