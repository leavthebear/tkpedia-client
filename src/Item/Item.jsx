import React from 'react';
import styled from 'react-emotion';
import Images from './Images';
import Brand from './Brand';
import NickNames from "./NickNames";
import Prices from "./Prices";
import Dates from "./Dates";
import Colors from "./Colors";
import SizeTable from "./SizeTable";
import Materials from "./Materials";
import UserTags from "./UserTags";
import Designers from "./Designers";
import {smallSpacing} from "../Common/Styles";

// https://1vfzs3.axshare.com/#g=1&p=home

const Container = styled('div')`
  display: flex;
  flex-direction: column;
`;

const Name = styled('h1')`
  margin: auto;
`;

const OtherName = styled('h2')`
  margin: auto;
`;

const Details = styled('div')`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-evenly;
`;

const TextDetailsContainer = styled('div')`
  flex-grow: 1;
  max-width: 624px;
  display: flex;
  flex-direction: column;
  margin: ${smallSpacing};
`;

const TextDetails = ({children}) => {
  return <TextDetailsContainer>
  {
    children.filter(n => n).reduce((prev, curr, index) => {
      return [prev, <Divider key={index}/>, curr];
    })
  }
</TextDetailsContainer>};


const Divider = styled('hr')`
  width: 100%;
`;


const Item = ({item}) => (<Container>
  <Name>{item.name}</Name>
  {item.otherNames.map((name, index) => <OtherName key={index}>{name}</OtherName>)}
  <Details>
    <Images images={item.images}/>
    <TextDetails>
      <Brand brand={item.brand}/>
      <NickNames nicknames={item.nicknames}/>
      <Prices prices={item.prices}/>
      {item.dates && item.dates.length > 0 && <Dates dates={item.dates}/>}
      <Colors colors={item.colors}/>
      <SizeTable sizeTable={item.sizeTable}/>
      {item.materials && item.materials.length > 0 && <Materials materials={item.materials}/>}
      {item.userTags && item.userTags > 0 && <UserTags userTags={item.userTags}/>}
      {item.designers && item.designers > 0 && <Designers designers={item.designers}/>}
    </TextDetails>
  </Details>
</Container>);


export default Item;