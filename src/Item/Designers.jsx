import React from 'react';
import styled from "react-emotion";
import LabeledRow from "./LabeledRow";
import {tinySpacing} from "../Common/Styles";

const AvatarContainer = styled('div')`
  display: flex;
  flex-direction: column;
  margin: ${tinySpacing};
`;

const AvatarImage = styled('img')`
  width: 4em;
  height: 4em;
  object-fit: contain;
`;

const AvatarName = styled('div')`
  width: 4em;
  overflow-wrap: break-word;
  text-align: center;
`;

const Avatar = ({name, image}) => <AvatarContainer>
  <AvatarImage src={image}/>
  <AvatarName>{name}</AvatarName>
</AvatarContainer>;

const Designers = ({designers}) => <LabeledRow label="设计师">
  {designers.map((designer, index) => <Avatar name={designer.name} image={designer.image}
                                              key={index}/>)}
</LabeledRow>;

export default Designers;