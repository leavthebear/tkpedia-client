import React, {Component} from "react";
import {getItem} from "./ItemAPI";
import Item from "./Item";

class ItemRoute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: null
    };
  }

  componentDidMount() {
    getItem(this.props.id).then(item => this.setState(prevState => ({
      ...prevState, item,
    })))
  }

  render() {
    return <div>
      {this.state.item && <Item item={this.state.item}/>}
    </div>
  }
}

export default ItemRoute