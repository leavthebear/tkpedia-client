import React, {Component} from 'react';
import styled, {css} from "react-emotion";
import {MediaImg} from "../Common/Media";

const Container = styled('div')`
  max-width: 320px;
  min-width: 160px;
  display: flex;
  flex-direction: column;
`;

const largeImageStyles = css`
  max-width: 320px;
  max-height: 400px;
  object-fit: contain;
`;

const Thumbnails = styled('div')`
  display: flex;
  flex-direction: horizontal;
  flex-wrap: wrap;
  align-content: flex-start;
`;

const thumbnailImageStyles = css`
  width: 64px;
  height: 80px;
  object-fit: contain;
`;

class Images extends Component {
  render() {
    return (<Container>
      <MediaImg styles={largeImageStyles} media={this.props.images[0]}/>
      <Thumbnails>
        {this.props.images.map((image,index) => <MediaImg key={index} styles={thumbnailImageStyles} media={image}/>)}
      </Thumbnails>
    </Container>);
  }
}

export default Images;