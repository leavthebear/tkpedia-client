import React from 'react';
import {css} from "react-emotion";
import LabeledRow from './LabeledRow';
import {MediaImg} from "../Common/Media";

const mediaImgStyles = css`
  width: 1.5em;
  height: 1.5em;
  object-fit: contain;
  align-self: center;
`;

const Brand = ({brand}) => <LabeledRow label={'品牌'}>
  <MediaImg media={brand.image} styles={mediaImgStyles}/>
  <div>{brand.name}</div>
</LabeledRow>;

export default Brand;