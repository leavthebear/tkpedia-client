import styled from "react-emotion";

const RowContainer = styled('div')`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: baseline;
`;

export default RowContainer;