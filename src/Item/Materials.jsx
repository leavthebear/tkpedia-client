import React from 'react';
import LabeledRow from "./LabeledRow";
import {join} from "./SeparatedList";

const Materials = ({materials}) => <LabeledRow label="材质">
  {join(materials.map((material, index) => <span key={index}>{material}</span>))}
</LabeledRow>;

export default Materials;