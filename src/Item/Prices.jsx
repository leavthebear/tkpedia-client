import React, {Component} from 'react';
import styled from "react-emotion";
import LabeledRow from "./LabeledRow";
import {smallSpacing} from "../Common/Styles";

const PriceContainer = styled('div')`
  margin-right: ${smallSpacing};
  color: lightpink;
  font-size: 2em;
`;

const Price = ({price}) => (<PriceContainer>
  <span>{price.amount}</span>
  <span>{price.currency}</span>
  <span>({price.type})</span>
</PriceContainer>);

class Prices extends Component {
  render() {
    return (<LabeledRow label={'售价'}>
      {
        this.props.prices.map((price, index) => <Price key={index} price={price}/>)
      }
    </LabeledRow>);
  }
}

export default Prices;