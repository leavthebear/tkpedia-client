import React from 'react';
import LabeledRow from './LabeledRow';
import {join} from "./SeparatedList";

const Date = ({date}) => (<div>
  <span>{date.timestamp.format("MM/DD/YYYY")}</span>
  <span>({date.note})</span>
</div>);

const Dates = ({dates}) => {
  return <LabeledRow label={'日期'}>
    {join(dates.map((date, index) => <Date key={index} date={date}/>))}
</LabeledRow>};

export default Dates;