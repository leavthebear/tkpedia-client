import React, {Component} from 'react';
import styled from "react-emotion";
import {largeSpacing} from "./Common/Styles";
import ItemThumbnailListRoute from "./ItemList/ItemThumbnailListRoute";
import {
  BrowserRouter as Router, Route, Link
} from 'react-router-dom'
import ItemRoute from "./Item/ItemRoute";

const Container = styled('div')`
  max-width: 960px;
  margin: ${largeSpacing} auto;
  display: flex;
  flex-direction: column;
`;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <Router basename="/tkpedia-client">
      <Container>
        <Link to="/items">Lists</Link>
        <Route exact path="/items" component={ItemThumbnailListRoute}/>
        <Route path="/items/:id" render={({ match }) => <ItemRoute id={match.params.id}/>}/>
      </Container>
    </Router>;
  }
}

export default App;
