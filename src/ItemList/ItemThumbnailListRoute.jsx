import React, {Component} from 'react';
import styled from "react-emotion";
import {borderWidth, tinySpacing} from "../Common/Styles";
import {listItems} from "../Item/ItemAPI";
import { Link } from 'react-router-dom'

const ItemThumbnailContainer = styled('div')`
  margin: ${tinySpacing};
  border-style: solid;
  border-width: ${borderWidth};
  padding: ${tinySpacing};
`;

const ItemThumbnail = ({item}) => (<ItemThumbnailContainer>
  <Link to={`/items/${item.id}`}>{item.name}</Link>
</ItemThumbnailContainer>);

const Container = styled('div')`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: baseline;
`;

class ItemThumbnailListRoute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [], nextPage: listItems, loading: false,
    };
  }

  componentDidMount() {
    this.loadMore()
  }

  loadMore() {
    if (this.state.loading || !this.state.nextPage) {
      return;
    }
    this.setState(prevState => ({...prevState, loading: true}), () => {
      this.state.nextPage().then(({items, nextPage}) => {
        console.log(items, nextPage);
        this.setState(prevState => ({
          ...prevState, items: prevState.items.concat(items), nextPage, loading: false,
        }));
      });
    });
  }

  render() {
    return <div>
      <Container>
        {this.state.items.map((item, index) => <ItemThumbnail item={item} key={index}/>)}
      </Container>
      <button onClick={this.loadMore.bind(this)}>load more</button>
    </div>;
  }
}

export default ItemThumbnailListRoute;