import React from "react";
//
// const Media = styled('img')`
//   width: 2em;
//   height: 2em;
//   object-fit: contain;
//   align-self: center;
// `;

// thumbnail: parseImageSize(json.media_details.sizes.thumbnail),
//     medium: parseImageSize(json.media_details.sizes.medium),
//     full: parseImageSize(json.media_details.sizes.full),
// function parseImageSize(json) {
//   if (json) {
//     return {
//       width: json.width, height: json.height, url: json.source_url,
//     };
//   }
//   return null;
// }

const formatSize = (size) => size.url + (size.width ? ` ${size.width}w` : '');

export const MediaImg = ({media, styles}) =>
    <img
        srcSet={[media.thumbnail, media.medium, media.full].filter(n => n).map(formatSize).join(", ")}
        className={styles}
        alt={JSON.stringify(media)}
    />;