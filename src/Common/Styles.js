export const tinySpacing = '4px';
export const smallSpacing = '8px';
export const largeSpacing = '16px';
export const borderWidth = '0.05em';
